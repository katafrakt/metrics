require 'nobrainer'
require './metric'

task :simulation do
  configure_nobrainer
  clear_db
  setup_metrics
  start_loop
end

task :watch do
  configure_nobrainer
  NoBrainer.run {|r| Metric.to_rql.changes()}.each do |change|
		type = change["old_val"]["type"]
    component = change["old_val"]["component"]
    old_val = change["old_val"]["cur_value"]
    new_val = change["new_val"]["cur_value"]

    puts "#{type} @ #{component}: ".ljust(35, ' ') + "#{old_val}".ljust(8, ' ') + "->   #{new_val}"
	end
end

def configure_nobrainer
  NoBrainer.configure do |config|
    config.app_name = "metrics_dev"
  end
end

def clear_db
  Metric.delete_all
end

def setup_metrics
  metrics = [
    {component: 'engine_1', type: 'temperature',
      min_value: 0, max_value: 100, cur_value: 30,
      likelihood_of_change: 5, amplitude_of_change: 3},
    {component: 'engine_2', type: 'temperature',
      min_value: 0, max_value: 100, cur_value: 55,
      likelihood_of_change: 2, amplitude_of_change: 3},
    {component: 'engine_1', type: 'plasma_level',
      min_value: 20, max_value: 80, cur_value: 77,
      likelihood_of_change: 1, amplitude_of_change: 1},
    {component: 'engine_2', type: 'plasma_level',
      min_value: 10, max_value: 30, cur_value: 30,
      likelihood_of_change: 1, amplitude_of_change: 10},
    {component: 'plasma_cannon', type: 'plasma_level',
      min_value: 10, max_value: 10000, cur_value: 5566,
      likelihood_of_change: 1, amplitude_of_change: 100},
    {component: 'water_tank', type: 'temperature',
      min_value: 0, max_value: 60, cur_value: 10,
      likelihood_of_change: 15, amplitude_of_change: 1}
  ]
  metrics.each {|m| Metric.create(m) }
end

def start_loop
  loop do
    Metric.all.each do |metric|
      perform_change(metric)
    end
    sleep 1
  end
end

def perform_change(metric)
  if rand(metric.likelihood_of_change) == 0
    amplitude = rand(metric.amplitude_of_change) + 1
    direction = [-1, 1].sample
    new_value = metric.cur_value += (direction * amplitude)
    new_value = metric.max_value if new_value > metric.max_value
    new_value = metric.min_value if new_value < metric.min_value
    metric.cur_value = new_value
    metric.save
  end
end
