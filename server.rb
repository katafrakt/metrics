require 'goliath'
require 'goliath/websocket'
require 'goliath/rack/templates'
require 'rethinkdb'
require 'json'

class Websocket < Goliath::WebSocket
  include Goliath::Rack::Templates
  include RethinkDB::Shortcuts
  use Rack::Static, :urls => ["/css"]

  def on_open(env)
    @database_thread ||= start_db_loop(env)
    env['subscription'] = env.channel.subscribe { |m| env.stream_send(m) }
  end

  def on_close(env)
    env.channel.unsubscribe(env['subscription'])
  end

  def response(env)
    if env['REQUEST_PATH'] == '/ws'
      super(env)
    else
      r.connect.repl
      metrics = r.db("metrics_dev_production").table('metrics').order_by('component', 'type').run
      [200, {}, erubis(:index, :views => Goliath::Application.root_path('views'), locals: {metrics: metrics})]
    end
  end

  def start_db_loop(env)
    Thread.new do
      r.connect.repl
      r.db("metrics_dev_production").table('metrics').changes().run.each do |change|
        msg = change.to_json.to_s
        env.channel << msg
      end
    end
  end
end
