class Metric
  include NoBrainer::Document

  #field :name, type: String
  field :component, type: String
  field :type, type: String

  field :min_value, type: Integer
  field :max_value, type: Integer
  field :cur_value, type: Integer

  field :likelihood_of_change, type: Integer
  field :amplitude_of_change, type: Integer
end
